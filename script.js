const numKey = Array.from(document.getElementsByClassName('key'));
const input = document.getElementsByClassName('input');
const clear = document.getElementById('clear');
const equal = document.getElementById('equal');

numKey.forEach(key => {
    key.addEventListener('click', () => {
        input.txt.value += key.innerText;
    })
});

clear.addEventListener('click', () => {
    input.txt.value = "";
})

equal.addEventListener('click', () => {
    input.txt.value = eval(input.txt.value);
})

window.onload = () => {
    input.txt.value = "";
}